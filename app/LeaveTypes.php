<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeaveTypes extends Model
{
    protected $fillable = ['name', 'numberOfDay', 'description'];
}
