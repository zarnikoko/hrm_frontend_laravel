<?php

namespace App\Http\Controllers;
use App\Http\Model\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Facades\JWTFactory;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Tymon\JWTAuth\Payload;
use Tymon\JWTAuth\Manager as JWT;
use GuzzleHttp\Exception\ServerException;
use Illuminate\Database\QueryException;

class AuthController extends Controller
{
    public function login(Request $request){
        $credentials = $request->json()->all();
        // echo "<pre>".var_export($credential,true)."</pre>";
        // exit();
        $validator = Validator::make($request->json()->all(),[
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|max:255',
        ]);
        if($validator->fails()){
            return response()->json(["message"=>$validator->errors()->toJson()],400);
        }
        $email = $request['email'];
        $emailExist = User::where('email', $email)->select("email")->first();
        if(!$emailExist){
            return response()->json(["message"=>'{"email":["Email does not exist!"]}'],400);
        }
        try{
            JWTAuth::factory()->setTTL(60*24); // Expired in 1 Day
            if(!$token = JWTAuth::attempt($credentials)){
                return response()->json(["message"=>'{"password":["Incorrect password!"]}'],400);
            }
        }catch(JWTException $error){
            return response()->json(["message"=>'{"tokken":["Could not create token!"]}'],400);
        }
        $user = auth()->user();
        return response()->json(compact('token','user'),202);
    }

    public function logout(){
        try{
            if(!$user = JWTAuth::parseToken()->authenticate()){
                return response()->json(['message'=>'user_not_found'],404);
            }
        }catch(TokenExpiredException $e){
            return response()->json(['message'=>'token_expired'],401);
        }catch(TokenInvalidException $e){
            return response()->json(['message'=>'token_invalid'],401);
        }catch(JWTException $e){
            return response()->json(['message'=>'token_absent'],401);
        }
        auth()->logout(true);
        return response()->json(['message'=>'logout_successful'],200);
    }
}
