<?php

namespace App\Http\Controllers;

use App\LeaveTypes;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class LeaveTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{
            $searchBy = strtolower($request['searchBy']);
            $searchWords = $request['searchWords'];
            $sortBy = $request['sortBy'];
            $sortType = $request['sortType'];
            $leave_types = DB::table('leave_types')
                           ->where($searchBy,'like','%'.$searchWords.'%')
                           ->orderBy($sortBy,$sortType)->get();
            return response()->json(['leaves' => $leave_types]);
        }catch(QueryException $e){
            return response()->json(['message'=>$e],500);
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // echo"<pre>".var_export($request,true)."</pre>";
        // exit();
        // return response()->json($request);

        $validator = Validator::make($request->json()->all(),[
            'name' => 'required|string|max:255|unique:leave_types',
            'code' => 'required|max:255|unique:leave_types',
            'type' => 'required',
            'unit' => 'required',
            'amount' => 'required|numeric|min:1',
        ]);

        if($validator->fails()){
            return response()->json(["message"=>$validator->errors()->toJson()],400);
        }
        try{
            $insertedID = DB::table('leave_types')->insertGetId([
                'name' => $request['name'],
                'code' => $request['code'],
                'type' => $request['type'],
                'unit' => $request['unit'],
                'amount' => $request['amount'],
                'description' => $request['description'],
                'upd_user' => auth()->user()->id,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
            $message = ($insertedID)?'success':'fail';
            $status = ($insertedID)?200:400;
            return response()->json(compact('insertedID','message'),$status);
        }catch(\Illuminate\Database\QueryException $e){
            return response()->json(['message' => $e]);
        }
       
        return response()->json(['message' => 'success']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LeaveTypes  $leaveTypes
     * @return \Illuminate\Http\Response
     */
    public function show(LeaveTypes $leaveTypes)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LeaveTypes  $leaveTypes
     * @return \Illuminate\Http\Response
     */
    public function edit(LeaveTypes $leaveTypes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LeaveTypes  $leaveTypes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LeaveTypes $leaveTypes)
    {
        $validator = Validator::make($request->json()->all(),[
            'name' => 'required|string|max:255',
            'code' => 'required|max:255',
            'type' => 'required',
            'unit' => 'required',
            'amount' => 'required|numeric|min:1',
        ]);

        if($validator->fails()){
            return response()->json(["message"=>$validator->errors()->toJson()],400);
        }
        DB::beginTransaction();
        try{
            $update = DB::table('leave_types')->where('id', $request['id'])->update([
                        'name' => $request['name'],
                        'code' => $request['code'],
                        'type' => $request['type'],
                        'unit' => $request['unit'],
                        'amount' => $request['amount'],
                        'description' => $request['description'],
            ]);
            if($update==1){
                DB::table('leave_types')->where('id', $request['id'])->update([
                   'upd_user' => auth()->user()->id,
                    'updated_at' => Carbon::now()
                ]);     
            }
            DB::commit();
            $message = 'success';
            $type= 'update';
            $status = ($update == 1)?200:400;
            return response()->json(compact('update','message','type'),$status);
        }catch(\Illuminate\Database\QueryException $e){
            DB::rollback();
            return response()->json(['message' => $e]);
        }
       
        return response()->json(['message' => 'success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LeaveTypes  $leaveTypes
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $delete = DB::table('leave_types')->where('id',$id)->delete();
            $message = ($delete==1)? 'success' : 'fail';
            $status =  ($delete==1)? 200 : 400;
            return response()->json(compact('delete','message'),$status);
        }catch(\Illuminate\Database\QueryException $e){
            return response()->json(["message"=>$id],400);
        }
    }
}