<?php

namespace App\Http\Controllers;
use App\Http\Model\User;
use App\Notifications\UserUpdateNotification;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request['limit'];
        $searchBy = strtolower($request['searchBy']);
        $searchWords = $request['searchWords'];
        $sortBy = $request['sortBy'];
        $sortType = $request['sortType'];
        // print_r($searchWords);
        // exit();
        $users = DB::table('users')
                ->where($searchBy,'like','%'.$searchWords.'%')
                ->orderBy( $sortBy,$sortType)
                ->paginate($limit);
        $message = "success";
    	return response()->json(compact('users','message'),202);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->json()->all(),[
            'name' => 'required|string|max:255|min:2',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'type' => 'required|string|max:255'
        ]);

        if($validator->fails()){
            return response()->json(["message"=>$validator->errors()->toJson()],400);
        }

        $user = User::create([
            'name' => $request->json()->get('name'),
            'email'=> $request->json()->get('email'),
            'password' => Hash::make($request->json()->get('password')),
            'type' => $request->json()->get('type'),
        ]);

        $message = "success";
        $type = "register";

        return response()->json(compact('user','message','type'),201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        echo "show";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->json()->all(),[
            'name' => 'required|string|max:255|min:2',
            'email' => 'required|string|email|max:255',
            'type' => 'required|string|max:255'
        ]);

        if($validator->fails()){
            return response()->json(["message"=>$validator->errors()->toJson()],400);
        }

        $email = $request['email'];
        $emailCheck = DB::table('users')
                        ->where('email', $email)
                        ->where('id','!=',$id)
                        ->select("email")->first();
        if($emailCheck){
            return response()->json(["message"=>'{"email":["The email has already been taken."]}'],400);
        }

        $to_updated = array(
            "name" => $request['name'],
            "email" => $request['email'],
            "type" => $request['type'],
        );

        if($request['resetPsw']){
            $resetPsw = Hash::make('123456');
            $to_updated['password'] = $resetPsw;
        }

        DB::beginTransaction();
        try {
            // Get the updated rows count here. Keep in mind that zero is a
            // valid value (not failure) if there were no updates needed
            $count = DB::table('users')
                        ->where('id',$id)
                        ->update($to_updated);
            if($count==1){
                DB::table('users')
                ->where('id',$id)
                ->update( ["updated_at" => Carbon::now()]);
                $notify_msg = [
                    'type' => 'user_update',
                    'msg' => 'Your account info have been changed .',
                    'updated_user' => auth()->user()->id,
                ];
                User::find($id)->notify(new UserUpdateNotification($notify_msg));
            }

            $message = 'success';
            $type = 'update';
            DB::commit();
            return response()->json(compact('count','message','type'),201);
        } catch (\Illuminate\Database\QueryException $e) {
            // Do whatever you need if the query failed to execute
            DB::rollback();
            return response()->json(["message"=>$e],400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $delete = DB::table('users')->where('id',$id)->delete();
            $message = ($delete==1)? 'success' : 'user_not_found';
            $status =  ($delete==1)? 200 : 400;
            return response()->json(compact('delete','message'),$status);
        }catch(\Illuminate\Database\QueryException $e){
            return response()->json(["message"=>$e],400);
        }
        
    }
}
