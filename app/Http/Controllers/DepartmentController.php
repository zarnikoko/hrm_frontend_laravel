<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;


class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $searchBy = strtolower($request['searchBy']);
        $searchWords = $request['searchWords'];
        $sortBy = $request['sortBy'];
        $sortType = $request['sortType'];
        try{
            $depts = DB::table('departments')
                    ->where($searchBy,'like','%'.$searchWords.'%')
                    ->orderBy($sortBy,$sortType)
                    ->get();
            $message = "success";
            return response()->json(compact('depts','message'),200);
        }catch(QueryException $e){
            return response()->json(['message'=>$e],500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validator = Validator::make($request->json()->all(),[
            'name' => 'required|string|max:255|unique:departments',
            'short_name' => 'required|string|max:255|unique:departments',
        ]);

        if($validator->fails()){
            return response()->json(["message"=>$validator->errors()->toJson()],400);
        }
        try{
            $insertedID = DB::table('departments')->insertGetId([
                'name' => $request['name'],
                'short_name' => $request['short_name'],
                'upd_user' => auth()->user()->id,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
            $message = ($insertedID)?'success':'fail';
            $status = ($insertedID)?200:400;
            $type = 'create' ; 
            return response()->json(compact('insertedID','message','type'),$status);
        }catch(\Illuminate\Database\QueryException $e){
            return response()->json(['message' => $e]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->json()->all(),[
            'name' => 'required|string|max:255',
            'short_name' => 'required|string|max:255',
        ]);

        if($validator->fails()){
            return response()->json(["message"=>$validator->errors()->toJson()],400);
        }
        $name = $request['name'];
        $shortName = $request['short_name'];
        $nameCheck = DB::table('departments')
                        ->where('name', $name)
                        ->where('id','!=',$id)
                        ->select("name")->first();
        $shortNameCheck = DB::table('departments')
                        ->where('short_name', $shortName)
                        ->where('id','!=',$id)
                        ->select("short_name")->first();

        if($nameCheck){
            $err_message['name'][0] = "The name has already been taken.";
        }
        if($shortNameCheck){
            $err_message['short_name'][0] = "The short name has already been taken.";
        }

        if(isset($err_message)){
            $err_json = json_encode($err_message);
            return response()->json(["message"=>$err_json],400);
        }

        $to_updated = array(
            "name" => $request['name'],
            "short_name" => $request['short_name'],
        );

        DB::beginTransaction();
        try {
            // Get the updated rows count here. Keep in mind that zero is a
            // valid value (not failure) if there were no updates needed
            $count = DB::table('departments')
                        ->where('id',$id)
                        ->update($to_updated);
            if($count==1){
                DB::table('departments')
                ->where('id',$id)
                ->update( [
                    "updated_at" => Carbon::now(),
                    "upd_user" => auth()->user()->id,
                ]);
            }
            $message = 'success';
            $type = 'update';
            DB::commit();
            return response()->json(compact('count','message','type'),201);
        } catch (\Illuminate\Database\QueryException $e) {
            // Do whatever you need if the query failed to execute
            DB::rollback();
            return response()->json(["message"=>$e],400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $delete = DB::table('departments')->where('id',$id)->delete();
            $message = ($delete==1)? 'success': 'fail';
            $status = ($delete==1)? 200 : 400;
            return response()->json(compact('delete','message'),$status);
        }catch(\Illuminate\Database\QueryException $e){
            return response()->json(["message"=>$id],400);
        }
    }
}
