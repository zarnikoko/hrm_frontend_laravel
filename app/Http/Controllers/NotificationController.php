<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Carbon;


class NotificationController extends Controller
{
    public function Get_Noti(Request $request){

        $unreadNotiCount  = auth()->user()->unreadNotifications->count(); // ->groupBy('type')
        $readNotiCount  = auth()->user()->readNotifications->count();

        // $firstTime = $request['firstTime'];
        $limit = 7 ;
        $status = $request['status'];

        $noti = DB::table('notifications')
                   ->where('notifiable_id',auth()->user()->id);
        if($status == 'unread'){
            $noti = $noti->whereNULL('read_at');
        }

        if($status == 'read'){
            $noti = $noti->whereNotNull('read_at');
        }
                  
        if($request['dateTime']!=''){
            $noti = $noti->where('created_at','<',$request['dateTime']);
            // $noti = $noti->where(function($q) use($request){
            //     $q->where(function($q){
            //         $q->whereNull('read_at');
            //     })->orWhere(function($q) use($request){
            //         $q->where('read_at', '<', $request['dateTime']);
            //     });
            // });
        }

        $noti = $noti
                ->orderBy('created_at','desc')
                ->paginate($limit);
                // ->orderByRaw("-read_at",'asc')

        $message = 'success';

        return response()->json(compact('message','unreadNotiCount','readNotiCount','noti'),202);

    }

    public function Get_Noti_By_ID($id){
        $unreadNotiCount  = auth()->user()->unreadNotifications->count(); // ->groupBy('type')
        $noti = DB::table('notifications')
                   ->where('id',$id)
                   ->first();
        $message = 'success';
        return response()->json(compact('message','unreadNotiCount','noti'),202);
    }

    public function All_Mark_As_Read(){
    	auth()->user()->unreadNotifications->map(function($unread){
    		$unread->markAsRead();
    	});
        $message = 'success';
        return response()->json(compact('message'),202);
    }

    public function Delete_All_Read(){
        DB::table('notifications')
        ->where('notifiable_id',auth()->user()->id)
        ->whereNotNull('read_at')
        ->delete();
    	$message = 'success';
        return response()->json(compact('message'),202);
    }

    public function Mark_As_Read($id){
        $count = DB::table('notifications')
        ->where('id',$id)
        ->update(['read_at'=>Date::now()]);
        $message = 'success';
        return response()->json(compact('message','count'),202);
    }

    public function Delete_Read($id){
        $count = DB::table('notifications')
        ->where('id',$id)
        ->whereNotNull('read_at')
        ->delete();
        $message = 'success';
        return response()->json(compact('message','count'),202);
    }

}
