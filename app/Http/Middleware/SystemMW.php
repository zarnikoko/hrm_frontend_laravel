<?php

namespace App\Http\Middleware;

use Closure;

class SystemMW
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user() && $request->user()->type != "system"){
            return response()->json(['message'=>'unauthorized_user'],401);
        }
            return $next($request);
    }
}
