<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LeaveSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('leave_types')->insert([
            'name' => 'Casural Leave',
            'amount' => 6,
            'code' => 'CL',
            'type' => 'paid',
            'unit' => 'days',
            'description' => 'Casural Leave'
        ]);

        DB::table('leave_types')->insert([
            'name' => 'Sick Leave',
            'amount' => 3,
            'code' => 'SL',
            'type' => 'paid',
            'unit' => 'days',
            'description' => 'Sick leave is a period of absence from work due to ill health. If the sick leave period exceeds 7 days then the employee is required to provide a fit note from a doctor.'
        ]);

        DB::table('leave_types')->insert([
            'name' => 'Medical Leave',
            'amount' => 6,
            'code' => 'MDL',
            'type' => 'paid',
            'unit' => 'days',
            'description' => 'Twelve (12) work weeks of leave in a 12-month period are provided for qualifying conditions under the Federal Family & Medical Leave Act (FMLA).Parental Leave: A faculty/exempt professional may request a personal leave with appropriate approvals.Serious Health Condition Leave: Shall be granted to eligible employees as a result of the employees serious health condition to care for his/her spouse, child or parent with a serious health condition as defined by FMLA.'
        ]);

        DB::table('leave_types')->insert([
            'name' => 'Maternity Leave',
            'amount' => 30,
            'code' => 'ML',
            'type' => 'paid',
            'unit' => 'days',
            'description' => 'When an employee takes time off to have a baby they are entitled to Statutory Maternity Leave which is 52 weeks long and made up of:
            Ordinary Maternity Leave - first 26 weeks
            Additional Maternity Leave - last 26 weeks
            They are not required to take the full Statutory Leave entitlement, however they must take 2 weeks’ leave after the baby is born or 4 weeks if they work in a factory. '
        ]);

        DB::table('leave_types')->insert([
            'name' => 'Bereavement Leave',
            'amount' => 1,
            'code' => 'BL',
            'type' => 'paid',
            'unit' => 'days',
            'description' => 'One (1) day paid leave granted to employees for the death of a family member.  May be extended to 3 days with the approval of Human Resources and the employing official.'
        ]);

        DB::table('leave_types')->insert([
            'name' => 'Civil Leave',
            'amount' => 5,
            'code' => 'CIL',
            'type' => 'unpaid',
            'unit' => 'days',
            'description' => 'Leave with pay granted to employees to serve on juries, as trial witnesses, or to exercise other subpoenaed civil duties.  All remuneration, exclusive of expenses, received by employee must be turned in to Plaza Cashier within 5 days of receipt.'
        ]);

        DB::table('leave_types')->insert([
            'name' => 'Disability Leave',
            'amount' => 30,
            'code' => 'DL',
            'type' => 'paid',
            'unit' => 'days',
            'description' => ' Leave shall be granted for a reasonable period to a permanent employee who is precluded from duties because of a disability (including those related to pregnancy or childbirth).  The disability shall be defined and certified by the employees licensed health care provider.'
        ]);

        DB::table('leave_types')->insert([
            'name' => 'Elective Office',
            'amount' => 15,
            'code' => 'EL',
            'type' => 'paid',
            'unit' => 'days',
            'description' => 'Faculty/Exempt Professionals at Western may serve in elective offices without change in their employment status so long as such activities do not interfere with their regular University duties and responsibilities.  In cases where there may be such interference, faculty/administrator shall request a leave of absence without pay from the President or Dean through the appropriate major administrative officer before filing for an elective office.'
        ]);

        DB::table('leave_types')->insert([
            'name' => 'Special Leave',
            'amount' => 60,
            'code' => 'SPEL',
            'type' => 'paid',
            'unit' => 'days',
            'description' => 'A faculty leave of short duration granted at the discretion of appropriate Dean.'
        ]);

        DB::table('leave_types')->insert([
            'name' => 'Professional Leave',
            'amount' => 365,
            'code' => 'PROL',
            'type' => 'paid',
            'unit' => 'days',
            'description' => 'After six years of service to the University, a tenured faculty/full-time exempt professional may apply to the President or Dean through the appropriate Vice President for professional leave to acquire knowledge and/or experience which will enhance the faculty/exempt professionals future contributions to the University.'
        ]);

        DB::table('leave_types')->insert([
            'name' => 'Educational Leave',
            'amount' => 365,
            'code' => 'EDUL',
            'type' => 'unpaid',
            'unit' => 'days',
            'description' => 'A faculty member who takes a leave without pay for study, scholarly or creative activity, or professional development may receive certain insurance and retirement benefits while on leave, provided that, in the opinion of the Dean and the President or Presidents designee the purposes of the leave directly benefit the Institution.Requests, by Exempt Professionals/Classified staff for educational leave (*leave without pay), must be submitted in writing and approved by the appropriate employing official.'
        ]);

        DB::table('leave_types')->insert([
            'name' => 'Personal Leave',
            'amount' => 7,
            'code' => 'PL',
            'type' => 'paid',
            'unit' => 'days',
            'description' => 'A faculty/exempt professional may request a personal leave with appropriate approvals.'
        ]);

        DB::table('leave_types')->insert([
            'name' => 'Worker Compensation',
            'amount' => 30,
            'code' => 'WCL',
            'type' => 'paid',
            'unit' => 'days',
            'description' => 'May be a combination of time loss compensation and accrued paid leave (VL, SL).  If sick leave is involved, employee must return overpayment or buy back sick leave when compensation adjustment is made by Labor and Industries.'
        ]);

        DB::table('leave_types')->insert([
            'name' => 'Unauthorised absence',
            'amount' => 3,
            'code' => 'UNA',
            'type' => 'unpaid',
            'unit' => 'days',
            'description' => 'Unauthorised absence is failing to turn up to work without having a statutory or contractual right, or the employer’s permission, to be absent i.e not for any of the other reasons stated in this article.
            Whilst there can be an exhaustive list of types of leave that your employees could take, these encapsulate the most common and genuine reasons for employee absence. It’s important for employers to have an understanding of these types of leave so that they can effectively manage absence within their small business. '
        ]);

        DB::table('leave_types')->insert([
            'name' => 'Parental leave',
            'amount' => 180,
            'code' => 'PAL',
            'type' => 'unpaid',
            'unit' => 'days',
            'description' => 'Eligible employees can take unpaid parental leave to look after their child’s welfare. This might include:
            Spending more time with their children
            Look at new schools
            Settle children into new childcare arrangements
            Spend more time with family, such as visiting grandparents'
        ]);
    }
}