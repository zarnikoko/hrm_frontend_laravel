<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Faker\Factory as Faker;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $type = ['system','admin'];

        foreach (range(1,50) as $index) {
            DB::table('users')->insert([
                'name' => $faker->name,
                'email' => $faker->unique()->safeEmail,
                'password' =>  Hash::make('654321'), // password
                'type' => $type[rand(0,1)],
            ]);
        }  
        
        DB::table('users')->insert([
            'name' => 'Zar Ni',
            'email' => 'zarni@gmail.com',
            'password' => Hash::make('654321'),
            'type' => 'system',
        ]);

        DB::table('users')->insert([
            'name' => 'Swe',
            'email' => 'swe@gmail.com',
            'password' => Hash::make('654321'),
            'type' => 'admin',
        ]);
    }
}
