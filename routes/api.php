<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Response;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
// Route::get('/user',function(Request $request){
//     $user = $request->user();
//     return response()
//     ->json(['status','success',compact('user')],202)
//     ->header('Content-Type', 'application/json');  
// });

Route::post('login','AuthController@login')->name('Login');
Route::post('logout','AuthController@logout')->name('Logout');

Route::group([
    'middleware' => ['auth:api'],
],function(){
    Route::get('get_noti', 'NotificationController@Get_Noti')->name('Get_Noti');
    Route::get('get_noti_by_id/{id}', 'NotificationController@Get_Noti_By_Id')->name('Get_Noti_By_Id');
    Route::put('all_mark_as_read', 'NotificationController@All_Mark_As_Read')->name('All_Mark_As_Read');
    Route::put('mark_as_read/{id}', 'NotificationController@Mark_As_Read')->name('Mark_As_Read');
    Route::put('delete_all_read', 'NotificationController@Delete_All_Read')->name('Delete_All_Read');
    Route::delete('delete_read/{id}', 'NotificationController@Delete_Read')->name('Delete_Read');
});


Route::group([
    'middleware' => ['auth:api','App\Http\Middleware\SystemMW'],
],function(){
    Route::resource('user', 'UserController');
});

Route::group([
    'middleware' => ['auth:api','App\Http\Middleware\AdminMW'],
],function(){
    Route::resource('leave_types', 'LeaveTypesController');
    Route::resource('department', 'DepartmentController');
});



